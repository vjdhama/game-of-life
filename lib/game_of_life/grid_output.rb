module GameOfLife
  class Grid
    attr_reader :size_of_grid

    def initialize(size_of_grid)
      @size_of_grid = size_of_grid
      @grid = Array.new(@size_of_grid) { |i| Array.new(@size_of_grid) { |i| '0' }}
    end

    def return_grid_state
      @grid
    end
  end
end