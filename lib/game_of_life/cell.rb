module GameOfLife
  class Cell 
    attr_reader :state

    DEAD = Object.new

    def initialize(state)
      @state = state
    end

    def dead?
      @state == DEAD
    end 
  end
end