require 'spec_helper'

module GameOfLife 
  describe Grid do
    describe '#output' do
      it 'return a square grid of game of life' do
        
        size_of_grid = 3
        expected_return_value = [['0','0','0'],['0','0','0'],['0','0','0']]
        
        grid = Grid.new(size_of_grid)
        
        expect(grid.return_grid_state).to eq(expected_return_value)
      end
    end
  end
end
