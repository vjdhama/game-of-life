require 'spec_helper'

module GameOfLife
  describe Cell do
    describe "#dead?" do
      it 'should check if cell is dead' do
        cell = Cell.new(Cell::DEAD)

        expect(cell).to be_dead
      end
    end
  end
end