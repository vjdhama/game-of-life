GAME OF LIFE A zero player evolution game by conway.

SETUP and BUILD

1. git clone git@github.com:vjdhama/game-of-life.git
2. bundle install
3. bundle exec rake
4. bundle exec rspec [SPECFILE_PATH:line]

RUN

ruby bin/gol.rb